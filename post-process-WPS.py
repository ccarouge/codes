#!/usr/bin/python

from pathlib import Path
import shutil, sys, os, argparse
import xarray as xr
import pandas as pd
import glob
import wrf
import netCDF4 as nc
import datetime as dt
import numpy as np


#
# To cut wrfbdy and wrflowinp files into monthly files.
# It will correctly take into account the 00am time
# of the next month (included in wrflowinp but excluded
# in wrfbdy)
#


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="Split WRF boundary condition files in time"
    )

    parser.add_argument(
        "-i",
        "--input_path",
        type=str,
        help="input directory path containing files to process",
    )
    parser.add_argument("-o", "--outdir", type=str, help="output directory path")

    return parser.parse_args(args)


def cut_files(inputdir, outdir, filename, strict):
    """It will cut the input files into monthly files
    filename: root of the file (wrfbdy or wrflowinp)
    strict: If True does not include the extra 00am at the end"""

    shift = 1
    if strict:
        shift = 0

    file_list = sorted(glob.glob(f"{inputdir}/{filename}_d??"))

    # Read times in netcdf file
    for nf in file_list:

        # String Split the filename
        txt = nf.split("_")

        # Get Times variable
        fi = nc.Dataset(nf)
        times = wrf.getvar(fi, "times", timeidx=None)

        # Open file using xarray
        fixr = xr.open_dataset(nf, engine="netcdf4")

        # Get year start indices
        tmp = pd.DatetimeIndex(times.values)
        _, yindex = np.unique(tmp.year, return_index=True)

        # Loop through the years
        for yy in range(len(yindex)):

            # Get month start indices
            ystart = yindex[yy]
            if len(yindex) == 1 or yy == (len(yindex) - 1):
                yend = -1
            else:
                yend = yindex[yy + 1]

            _, mindex = np.unique(tmp.month[ystart:yend], return_index=True)

            # Get the indexes of the different months
            ind0 = 0
            while ind0 != (len(mindex)):
                starting = ystart + mindex[ind0]
                if ind0 == (len(mindex) - 1):
                    ending = yend
                else:
                    ending = ystart + mindex[ind0 + 1] + shift
                subset = fixr.isel(Time=slice(starting, ending))
                subset.to_netcdf(
                    f"{outdir}/{filename}_{txt[-1]}_{tmp.year[starting]}-{format(tmp.month[starting], '02d')}",
                    mode="w",
                    format="NETCDF4",
                )
                ind0 += 1

            del mindex, ind0


def main():

    # Get directory paths from arguments
    args = parse_args(sys.argv[1:])

    inputdir = Path(args.input_path)
    print("Input directory is: %s" % (inputdir))

    outdir = Path(args.outdir)
    print("Output directory is: %s" % (outdir))

    # Deal with wrfbdy.
    strict = False  # Include 00 from next month
    cut_files(inputdir, outdir, "wrfbdy", strict)

    # Deal with wrflowinp
    strict = False  # Include 00 from next month
    cut_files(inputdir, outdir, "wrflowinp", strict)


if __name__ == "__main__":
    main()

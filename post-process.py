#!/usr/bin/python

from pathlib import Path
import shutil, sys, os, argparse
import xarray as xr
import pandas as pd
import splitvar
import tempfile
import climtas

soil_dims_names = [
    "RelSMC_profiles",
    "SoilMoist_profiles",
    "SoilTemp_profiles",
    "SmLiqFrac_profiles",
    "SmFrozFrac_profiles",
]


def parse_args(args):
    parser = argparse.ArgumentParser(description="Concatenate LIS output files in time")

    parser.add_argument("--verbose", help="Verbose output", action="store_true")
    parser.add_argument(
        "input_path",
        metavar="path",
        type=str,
        help="Directory containing original LIS output files",
    )
    parser.add_argument(
        "--orig_levels",
        help="Leave all profiles dimensions as output per LIS",
        action="store_true",
    )
    parser.add_argument(
        "-f",
        "--frequency",
        help="Time period to group for output",
        default="M",
        action="store",
    )
    parser.add_argument(
        "--timeformat",
        help="strftime format string for date fields in filename",
        default="%Y%m",
    )
    parser.add_argument("-o", "--outdir", help="output directory path")
    parser.add_argument(
        "-s", "--start", type=str, help="first year and month", metavar="YYYY-MM"
    )
    parser.add_argument(
        "-e", "--end", type=str, help="last year and month", metavar="YYYY-MM"
    )
    parser.add_argument(
        "-d", "--dom", type=int, help="domain number as an integer value"
    )

    return parser.parse_args(args)


def copy_to_tmp(dwd):

    tmp = tempfile.TemporaryDirectory()

    list_of_files = list(dwd.glob("LIS_HIST*"))
    if list_of_files is not None:
        for ff in list_of_files:
            shutil.copy(ff, tmp.name)
    else:
        raise "No files to process"
    return tmp


def LIStimeoffset(time, dir=-1):
    """Get the timestep and offset the values by half the timestep
    time: DataArray time array to work with. We assume the dates are evenly spaced and increasing
    dir: 1 or -1 To add or subtract the offset"""

    dt = (time[1] - time[0]) / 2.0
    return time + dir * dt


def rename_profiles(ds):
    """Rename the *_profiles dimensions to soil_layers. And take the SOIL_LAYER_THICKNESSES file argument
    as coordinate for soil_layers"""

    prof_dims = {}
    # Find all the dimensions with _profiles in their names
    for k in ds.dims.keys():
        if k in soil_dims_names:
            prof_dims[k] = "soil_layers"

    if len(prof_dims) != 0:
        # Only rename and assign coordinate if we found
        # dimensions to rename and prof_dims isn't empty.
        ds = ds.rename(prof_dims)

        # Get coordinate
        soil_layers = xr.DataArray(
            ds.SOIL_LAYER_THICKNESSES,
            dims="soil_layers",
            attrs={"standard_name": "soil_layer_thicknesses"},
        )
        ds = ds.assign_coords({"soil_layers": soil_layers})

    return ds


def files_to_read(cdate, tmp, LISHIST_files, dom):

    fdate = cdate.strftime("%Y%m")
    fglob = f"LIS_HIST_{fdate}*.d0{dom}.nc"
    tmplist = sorted(list(Path(tmp.name).glob(fglob)))
    # First file of next month:
    ndate = cdate + 1
    fdate = ndate.strftime("%Y%m")
    fglob = list(Path(tmp.name).glob(f"LIS_HIST_{fdate}*.d0{dom}.nc"))
    tmplist.append(sorted(fglob)[0])

    # Remove any file that was processed at the previous step
    # This is because LIS timestamp is at the end of the timestep
    # The first file of a month should be concatenated with the previous month
    final_list = [filename for filename in tmplist if filename not in LISHIST_files]

    return final_list


def main():

    args = parse_args(sys.argv[1:])

    # Initialise year and month variables
    cdate = pd.to_datetime(args.start).to_period(
        "m"
    )  # Get the date with a monthly precision
    edate = pd.to_datetime(args.end).to_period("m")

    # Store initial directory to move around the directory tree.
    input_path = Path(args.input_path)
    print("Input directory: ", input_path)

    tmp = copy_to_tmp(input_path)

    LISHIST_files = []
    while cdate < edate:

        print(f"Handling month {cdate}")
        LISHIST_files = files_to_read(cdate, tmp, LISHIST_files, args.dom)
        # Read files with xarray
        ds = xr.open_mfdataset(LISHIST_files, chunks={"time": 15})
        # Get the "*profile" dimensions to be "soil_layer"
        # Get the soil layer coordinate from file attribute: SOIL_LAYER_THICKNESSES
        if not args.orig_levels:
            ds = rename_profiles(ds)

        # Change the time coordinate to use the middle of the timestep
        ds["time"] = LIStimeoffset(ds.time)

        # Change the lat/lon to remove the time dimension.
        ds["lat"] = ds["lat"].isel(time=0, drop=True)
        ds["lon"] = ds["lon"].isel(time=0, drop=True)
        ds = ds.assign_coords({"lon": ds["lon"], "lat": ds["lat"]})

        fdate = splitvar.format_date(ds["time"].values[0], "%Y%m")
        filename = Path(args.outdir) / f"LIS.CABLE.{fdate}-{fdate}.d0{args.dom}.nc"
        climtas.io.to_netcdf_throttled(ds, filename)

        # Next month
        cdate = cdate + 1


if __name__ == "__main__":
    main()

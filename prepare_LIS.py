#!/usr/bin/python

# ===================================================================================
#
# Script to prepare a LIS spin-up run previous to a LIS-WRF simulation. This script
# prepares:
#
# - monthly lis.config files for the spin-up (takes the grid info from the geo_em
#   file specified by the user.
# - the script to launch the monthly LIS simulations in sequence
# - the lis.config for the WRF simulation
# - makes sure the INC_WATER_PTS pre-processor is defined in the make/misc.h or
#   make/LIS_misc.h file
# - compile LIS (clean + compile) before running the first month if asked for
# - makes sure the simulation will output Landcover, Soiltype and Elevation (in
#   the model output file).
#
#
# This script reads input in "input_scripts.txt" which must be present in the
# same directory as this script.
# ===================================================================================

import os, shutil
import os.path
import string
import sys
import copy
import WRF_LIS_lib


def LDT_postlis(input_dic):
    """To prepare the LDT config file after a LIS simulation before real.exe"""

    gridtype = input_dic["gridtype"]

    tmp = WRF_LIS_lib.get_grid(
        "LIS", gridtype, "{GEO_PATH}".format(**input_dic), input_dic["max_dom"]
    )
    input_dic.update(tmp)

    # We are always in nuwrf/coupled mode
    # Link to LIS output directory into LDTpostlis run directory
    WRF_LIS_lib.create_symlink(input_dic["LISOUT_PATH"], input_dic["LDTpostlis_PATH"])
    WRF_LIS_lib.create_symlink(input_dic["BDYDATA_PATH"], input_dic["LDTpostlis_PATH"])
    print(input_dic["LIS_PATH"])
    WRF_LIS_lib.create_symlink(input_dic["LIS_PATH"], input_dic["LDTpostlis_PATH"])

    start_year = input_dic["start_year"]
    start_month = input_dic["start_month"]
    tmp = WRF_LIS_lib.set_pndate(start_year, start_month, 1, input_dic)
    input_dic.update(tmp)

    WRF_LIS_lib.files_to_update("LDTpostlis", input_dic)
    # Need config and run files in the LDTprelis run directory
    WRF_LIS_lib.create_symlink(
        os.path.join(input_dic["DECK_PATH"], "ldt.config.postlis"),
        input_dic["LDTpostlis_PATH"],
    )
    WRF_LIS_lib.create_symlink(
        os.path.join(input_dic["DECK_PATH"], "run_LDTpostlis.sh"),
        input_dic["LDTpostlis_PATH"],
    )


def LDT_prelis(input_dic):
    """To prepare the LDT config file before a LIS offline simulation"""

    changes = input_dic

    # Add COUPLED = integer form of coupled
    input_dic["COUPLED"] = int(input_dic["mode"].lower() == "nuwrf")

    #    # Add PARAMSDIR to be the link name for the LIS input files
    #    input_dic['PARAMSDIR'] = 'LIS_PARAMS'

    # Create link in run directory
    WRF_LIS_lib.create_symlink(input_dic["LISPARAMSDIR"], input_dic["LDTprelis_PATH"])

    # Link to LIS run directory into LDTprelis run directory.
    WRF_LIS_lib.create_symlink(input_dic["LIS_PATH"], input_dic["LDTprelis_PATH"])

    # -----------------------------------------------------------------------------------
    # Read the grid info from the geo_em.d01.nc file first if coupled simulation.
    # Else the info must already be in template_lis.config
    if input_dic["coupled"]:
        gridtype = input_dic["gridtype"]

        tmp = WRF_LIS_lib.get_grid(
            "LIS", gridtype, "{GEO_PATH}".format(**input_dic), input_dic["max_dom"]
        )
        input_dic.update(tmp)
    else:
        print("Reminder: The grid info must be hard-wired in template_lis.config")
    # End of if (coupled)

    # Get the real files from the template files
    WRF_LIS_lib.files_to_update("LDTprelis", input_dic)

    # Need config and run files in the LDTprelis run directory
    WRF_LIS_lib.create_symlink(
        os.path.join(input_dic["DECK_PATH"], "ldt.config.prelis"),
        input_dic["LDTprelis_PATH"],
    )
    WRF_LIS_lib.create_symlink(
        os.path.join(input_dic["DECK_PATH"], "run_LDTprelis.sh"),
        input_dic["LDTprelis_PATH"],
    )


def prepare_LIS(input_dic):

    # Choose LIS_nmonths for the number of months for the LIS run
    input_dic["nmonths"] = input_dic["LIS_nmonths"]

    # Add COUPLED = integer form of coupled
    input_dic["COUPLED"] = int(input_dic["coupled"])

    #    # Add PARAMSDIR to be the link name for the LIS input files
    #    input_dic['PARAMSDIR'] = 'LIS_PARAMS'

    # Create link in run directory
    WRF_LIS_lib.create_symlink(input_dic["LISPARAMSDIR"], input_dic["LIS_PATH"])

    # A few initialisations
    days_in_month = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

    start_year = input_dic["start_year"]
    start_month = input_dic["start_month"]
    end_year = input_dic["end_year"]
    end_month = input_dic["end_month"]
    len_spinup = input_dic["len_spinup"]

    # spinup_dates is a dictionary with the start year/month and end year/month of the offline simulation
    if input_dic["coupled"]:
        sspin_year = start_year - len_spinup
        sspin_month = start_month

        espin_year = start_year
        espin_month = start_month

    else:
        sspin_year = start_year
        sspin_month = start_month

        espin_year = end_year
        espin_month = end_month + 1
        if espin_month > 12:
            espin_year = espin_year + 1
            espin_month = 1

    print("start year/month: {0} {1}".format(sspin_year, sspin_month))
    year = sspin_year
    month = sspin_month

    # Get a new dictionary for the changes
    changes = input_dic

    # -----------------------------------------------------------------------------------
    # Set time-invariant entries

    # Get lis.config file name
    changes["LISconfig"] = "lis.config"

    # For the run script
    XPROC = input_dic["XPROC"]
    YPROC = input_dic["YPROC"]
    changes["NPROCS"] = str(XPROC * YPROC)

    # Build FORC_LOC_DIR_AR needed for lis.config. It's only FORC_LOC_DIR repeated # of nests times.
    changes["FORC_LOC_DIR_AR"] = changes["max_dom"] * (changes["FORC_LOC_DIR"] + " ")

    #
    changes["RUNMODE"] = "retrospective"
    changes["TIMESTEP"] = input_dic["LIS_ts"] + "ss"
    if input_dic["FORC_DATA_NAME"] in ["PRINCETON"]:
        changes["REFHGT"] = 40
    elif input_dic["FORC_DATA_NAME"] in ["ERA-Interim"]:
        changes["REFHGT"] = 10

    # For the run script
    changes["DOCOMPILE"] = int(input_dic["compile"])

    # Time overlap: only needed for WRF, so always 0 here.
    if not ("OLAP" in changes):
        changes["OLAP"] = 0

    # -----------------------------------------------------------------------------------
    # Before loop: open the template lis.config to update the number of processors.
    # Then use this updated version to create the lis.config for each month of the spin-up.

    # Open the template for reading, a temporary file for writing and
    # lis.config to use when coupled to WRF for writing.
    tconfig = open("../templates/template_lis.config", "r")
    # trun = open('../template_run_LIS.csh','r')

    for line in tconfig:

        #    # Create the lis.config for the wrf simulation
        #    # Apply the changes with template
        #    line = string.Template(line).safe_substitute(changes)

        # Check if the LSM asked in template_lis.config correspond with input_scripts.txt data.
        if line.find("Land surface model:") != -1:
            tmp = line.split("#")
            tmp = tmp[0].split(":")
            if tmp[1] == "17" and changes["LSM"] != "CABLE":
                message = "Inconsistency for LSM to use between input_scripts.txt and template_lis.config"
                sys.exit(message)
            elif tmp[1] == "1" and changes["LSM"] != "NOAH":
                message = "Inconsistency for LSM to use between input_scripts.txt and template_lis.config"
                sys.exit(message)

        # Check if using supplemental forcing and if base forcing = fake forcing
        if line.find("Base forcing source:") != -1:
            tmp = line.split("#")
            tmp = tmp[0].split(":")
            if tmp[1].strip() == "0":  # Using fake forcing
                changes["BASE"] = 0
            else:
                changes["BASE"] = 1

        if line.find("Number of supplemental forcing sources:") != -1:
            tmp = line.split("#")
            tmp = tmp[0].split(":")
            if tmp[1].strip() == "0":  # Not using supp. forcing
                changes["SUPP"] = 0
            else:
                changes["SUPP"] = 1

    # # Check for misc.h or LIS_misc.h to include water points before compilation
    # # Give a value to H_File in any case to keep cat happy.
    # changes['H_FILE'] = 'LIS_misc.h'
    # if input_dic['compile'] and input_dic['coupled']:
    #     path=changes['CCRCLIS_DIR']+'/src/make/'

    #     if os.path.isfile(path+'misc.h'):
    #         misc_name=path+'misc.h'
    #     elif os.path.isfile(path+'LIS_misc.h'):
    #          misc_name=path+'LIS_misc.h'
    #     else:
    #         message= 'Missing the misc.h or LIS_misc.h file'
    #         message=message+' in src/make/'
    #         sys.exit(message)

    #     tmisc=open(misc_name,'r')
    #     outmisc=open('{DECK_DIR}/tempo'.format(**changes),'w')

    #     for line in tmisc:
    #         if line.find('INC_WATER_PTS') != -1:
    #             line=line.replace('undef','define')

    #         outmisc.write(line)
    #     tmisc.close()
    #     outmisc.close()
    #     changes['H_FILE'] = os.path.basename(tmisc.name)
    #    os.rename(outmisc.name,'{DECK_DIR}/{H_FILE}'.format(**changes))

    # Prepare model output file.
    # The entry in input_scripts.txt is in the form: filenames+path for all nests separated by a space.
    # Need to separate filename and pathname.
    # then a loop over the nests and for each nest check that the model output file will output
    # Soiltype, Landcover and Elevation
    tempo = changes["MODEL_OUTPUT_AR"].split(" ")
    if len(tempo) != changes["max_dom"]:
        message = "The number of filenames in MODEL_OUTPUT_AR must equal max_dom in input_scripts.txt"
        sys.exit(message)

    for i in range(changes["max_dom"]):
        tempo[i] = tempo[i].strip().strip("'")
        changes["MODELOUT_FILE_AR"] = os.path.basename(tempo[i])
        changes["MODELOUT_PATH"] = os.path.dirname(tempo[i])
        inmodel = open("../templates/{MODELOUT_FILE_AR}".format(**changes), "r")
        outmodel = open("{DECK_PATH}/{MODELOUT_FILE_AR}".format(**changes), "w")
        for line in inmodel:
            if changes["coupled"] and (
                line.find("Landcover") != -1
                or line.find("Soiltype") != -1
                or line.find("Elevation") != -1
            ):
                col0 = line.find("0")
                col1 = line.find("1")
                if col0 != -1 and col0 < col1:  # The first number is 0.
                    # It should be 1 to output this variable
                    tmp = list(line)
                    tmp[col0] = "1"
                    line = "".join(tmp)

            # Print all lines to mout
            outmodel.write(line)

        # Close files
        outmodel.close()
        inmodel.close()

    # Get all the filenames into a string for transfer and lis.config
    changes["MODELOUT_FILES_AR"] = " ".join(map(os.path.basename, tempo))
    changes["OUTPUT_FILE_AR"] = " ".join(map(repr, tempo))

    # -----------------------------------------------------------------------------------
    #                                                         MONTHLY CHANGES
    # -----------------------------------------------------------------------------------

    # -----------------------------------------------------------------------------------
    # Close the tconfig file to read again from the beginning for each month of the
    # spin-up
    tconfig.close()

    # -----------------------------------------------------------------------------------
    # set up loop to be over all months in the spin-up years
    while year < espin_year or (year == espin_year and month < espin_month):

        changes["SDAY"] = 1
        changes["NDAY"] = 1

        print("Prepare year/month:" + str(year) + "/" + str(month))
        print(str(espin_year) + " " + str(espin_month))
        # Is it February of a leap year?
        leapyear = int(
            (month == 2 and ((year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)))
        )

        # Get the RESTART value
        if changes["first"]:
            changes["RESTART"] = "coldstart"
        else:
            changes["RESTART"] = "restart"

        changes["FIRST"] = int(changes["first"])

        tmp = WRF_LIS_lib.set_pndate(year, month, changes["LIS_nmonths"], changes)
        changes.update(tmp)

        changes["SDAY"] = format(changes["SDAY"], "02d")
        changes["NDAY"] = format(changes["NDAY"], "02d")

        # Build the array of restart file names for LIS
        changes["RESTART_FILE_AR"] = [
            "LIS_RST_{LSM}_{SYEAR}{SMONTH}010000.d01.nc".format(**changes)
        ]
        for i in range(input_dic["max_dom"] - 1):
            grid = str(i + 2).rjust(2, "0")
            changes["RESTART_FILE_AR"] = changes["RESTART_FILE_AR"] + [
                "LIS_RST_{LSM}_{SYEAR}{SMONTH}010000.d".format(**changes)
                + grid
                + ".nc".format(**changes)
            ]

        # Join in a string
        changes["RESTART_FILE_AR"] = " ".join(changes["RESTART_FILE_AR"])

        # Get the restart interval if auto.
        # Is it February of a leap year?
        leapyear = int(
            (
                month == 2
                and (((year % 4 == 0) and (year % 100 != 0)) or year % 400 == 0)
            )
        )

        changes["RES_LIS_AR"] = (
            input_dic["max_dom"]
            * (str((days_in_month[month - 1] + 1 * leapyear) * 24 * 60 * 60) + " ")
        ).strip()

        # Update the CO2 concentration in the lis.config file
        co2year = WRF_LIS_lib.get_co2_conc(input_dic, year)
        if changes["LIS_nmonths"] > 12:
            co2espin_year = WRF_LIS_lib.get_co2_conc(input_dic, espin_year)
            changes["CO2CONC"] = (co2year + co2espin_year) / 2.0
        else:
            changes["CO2CONC"] = co2year

        # Open the lis.config for each month
        changes["LIS_CONFIG"] = "{LISconfig}_{SYEAR}_{SMONTH}".format(**changes)
        # outconfig = open('{DECK_DIR}/{LIS_CONFIG}'.format(**changes),'w')

        # for line in tconfig:

        #     #Update the variable fields to build the lis.config for spin-up
        #     line = string.Template(line).safe_substitute(changes)

        #     outconfig.write(line)

        WRF_LIS_lib.files_to_update("LIS", input_dic)
        ## Open the run script for each month
        # outrun = open('{DECK_DIR}/run_LIS_{SYEAR}_{SMONTH}'.format(**changes),'w')

        ## make sure the output is executable by user
        # os.fchmod(outrun.fileno(),0744)

        # for line in trun:
        #    #Update the variable fields to build the lis.config for spin-up
        #    line = string.Template(line).safe_substitute(changes)
        #
        #    outrun.write(line)

        # -----------------------------------------------------------------------------------
        # Prepare for next month

        # Rewind and close the config scripts
        # tconfig.seek(0)   # Rewind the template config for next month
        # outconfig.close()

        ## Rewind and close the run scripts
        # trun.seek(0)
        # outrun.close()

        # Copy outrun to the LIS run directory for the first period
        if changes["first"]:
            WRF_LIS_lib.create_symlink(
                os.path.join(
                    changes["DECK_PATH"], "run_LIS_{SYEAR}_{SMONTH}".format(**changes)
                ),
                changes["LIS_PATH"],
            )

        # -----------------------------------------------------------------------------------
        # Iterate for next month

        # Increment first
        changes["first"] = False
        changes[
            "DOCOMPILE"
        ] = 0  # We never want to compile for more than the first month!

        # Get next month and next year
        month = int(changes["NMONTH"])
        year = int(changes["NYEAR"])

    # End of the loop over the months
    # -----------------------------------------------------------------------------------

    # Copy post-process.py to the deck directory

    shutil.copy("post-process.py", "{DECK_PATH}/".format(**changes))

    # Clean up
    # tconfig.close()  # Close the tconfig file
    # trun.close()
